#!/usr/bin/python3

import subprocess
import time
import json

class ProjectBuilder:
    def __init__(self):
        self.file_list = []
        self.projects = []

    def get_list(self):
        out, err = subprocess.Popen(["aws", "s3", "ls", "s3://projects.bytepen.com", "--recursive", "--summarize"],stdout=subprocess.PIPE).communicate()
        for line in out.splitlines():
            line = str(line, 'utf-8')
            if line.startswith("20"):
                self.file_list.append(line.split()[-1])

    def get_projects(self):
        for file in self.file_list:
            if file.endswith("project-info/info.json"):
                # Standard Base is 50. This can be changed with a <score>.base file.
                # When a project is pushed, a <epoch>.push file is created.
                # A project gets 30 points for projects pushed today and one point is deducted each day.
                # Minimum score is base - 30.
                project = file.split('/')[0]
                base = 50
                now = int(time.time())
                push = now - 5184000 # 60 Days ago
                for c in self.file_list:
                    if c.startswith(project):
                        if c.endswith('.base'):
                            base = int(c.split('/')[-1].replace('.base',''))
                        elif c.endswith('.push'):
                            push = int(c.split('/')[-1].replace('.push',''))
                score = base + (30 - int((now - push) / (60*60*24)))
                if score < base - 30:
                    score = base - 30
                self.projects.append({"name":file.split('/')[0],"score":score})

    def output_projects(self):
        self.projects.sort(key=lambda x: x['score'], reverse=True)
        project_list = []
        for project in self.projects:
            project_list.append(project['name'])
        with open('public/projects.json', 'w') as f:
            json.dump({"projects": project_list}, f)

if __name__ == "__main__":
    pb = ProjectBuilder()
    pb.get_list()
    pb.get_projects()
    pb.output_projects()

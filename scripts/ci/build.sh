#!/bin/bash

set -e

main() {
    setup
	build
}

setup() {
	BASE_DIR="$(dirname $(cd "$(dirname $( cd "$(dirname "$0")" ; pwd -P ))"; pwd -P))"

	pushd "${BASE_DIR}"
		yarn install
		rsync -a node_modules /cache/node_modules/ &
	popd
}

build() {
	pushd "${BASE_DIR}"
		yarn lint
		yarn build
	popd
}

main "${@}"
